class nodejspack {
  class { 'nodejs':
    repo_url_suffix => '16.x',
  }

  package { ['pm2', '@vue/cli']:
    ensure   => 'present',
    provider => 'npm',
  }

}
