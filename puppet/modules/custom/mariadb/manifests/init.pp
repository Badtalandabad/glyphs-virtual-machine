class mariadb {
  class {'::mysql::server':
    package_name     => 'mariadb-server',
    restart          => true,
    service_name     => 'mysql',
    root_password    => 'root',
    override_options => {
      mysqld => {
        bind-address => '0.0.0.0',
        character-set-server => 'utf8mb4',
        collation-server     => 'utf8mb4_unicode_ci',
      },
    }
  }

  class {'::mysql::client':
    package_name => 'mariadb-client',
  }

  package { ['libmariadb3', 'libmariadb-dev']:   
    ensure => installed,
  }

  service { 'mariadb':
    ensure  => running,
    require => [Class['::mysql::server'], Class['::mysql::client']]
  }
  
  mysql::db { '*':
    user     => 'root',
    password => 'root',
    host     => '%',
    grant    => ['ALL'],
  }

  file { 'copy dump':
    path => '/tmp/db.sql',
    ensure => present,
    source => "puppet:///modules/$module_name/db.sql",
  }~>
  mysql::db { $project:
    user     => $project,
    password => $project,
    charset  => 'utf8mb4',
    collate  => 'utf8mb4_unicode_ci',
    sql      => '/tmp/db.sql',
  }


}
